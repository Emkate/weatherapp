$(function(){

    var cTemp = 0;
    var fTemp = 0;
    var actualTemp = "C";

    /**
     * @return {number}
     */
    function Round(n, k)
    {
        var factor = Math.pow(10, k);
        return Math.round(n*factor)/factor;
    }

    function weatherEvent(weather){
        switch(weather){
            case "Clouds":
                return "images/cloud.png";
            case "Rain":
                return "images/rainycloud.png";
            case "Snow":
                return "images/snowycloud.png";
            case "Clear":
                return "images/clear.png";
            default:
                return "images/falloutguy.png";
        }
    }

    navigator.geolocation.getCurrentPosition(function(position) {
        var str = "http://api.openweathermap.org/data/2.5/weather?lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&APPID=e735ba785f29693321b457345f7f46d2";

        $.getJSON(str, function(json) {

            cTemp = Round(json.main.temp - 273.15, 1);
            fTemp = Round(json.main.temp*1.8 - 459.67, 1);

                $("#temperature").empty().append("<h2>" + cTemp + "°C</h2>").hide().fadeIn(1500, function(){
                    $("#changeWeather").fadeIn(1500);
                    $("#geolocation").empty().append("<h2>" + json.name + "</h2>").hide().fadeIn(1500);
                    $("#weatherDiv").append('<img id="weatherImage" src="' + weatherEvent(json.weather[0].main) + '" class="center-block animated fadeInUp" style="display: none;"/>');
                    $("#weatherImage").fadeIn(1500);
                });
        });
    });

    $("#changeWeather").click(function(){
       if(actualTemp==="C"){
           $("#temperature").empty().append("<h2>" + fTemp + "°F</h2>");
           actualTemp="F";
       }
        else if(actualTemp==="F"){
           $("#temperature").empty().append("<h2>" + cTemp + "°C</h2>");
           actualTemp="C";
       }
    });

});
